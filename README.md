# bodhala-petl

A fork of the excellent Python PETL project. Primary enhancement is a more flexible
user feedback mechanism.

See the original alimanfoo PETL project for it's primary source.

- https://github.com/alimanfoo/petl

